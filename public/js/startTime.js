function startTime(user_id) {
    let currentDate = new Date().getDate();
    let total = $('#user-' + user_id + '-day-' + currentDate + '-total');
    $.post('/start-time', {user_id})
        .done((res) => {
            console.log(res);
            let userTime = $(`<div style="white-space: nowrap" id="time-${res.time_id}">${res.started_at} - </div>`);
            if (total[0]) {
                userTime.insertBefore(total);
            } else {
                document.getElementsByClassName('user '+user_id)[currentDate].append(userTime[0]);
            }
            $('#started-at').text('started at: ' + res.started_at);
            $('#started-at').css('display', 'inline-block');
            $('#start-time-'+user_id).css('display', 'none');
            $('#stop-time-'+user_id).css('display', 'inline-block');
            $('#time_id').val(res.time_id);
        })
        .fail((err) => {
            showErrMsg('Error');
        })
}