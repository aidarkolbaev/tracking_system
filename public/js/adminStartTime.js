function adminStartTime(user_id) {
    $.post('/start-time', {user_id})
        .done((res) => {
            window.location.reload();
        })
        .fail((err) => {
            showErrMsg('Error during start')
        })
}