function stopTime(user_id, timeId = null) {
    let time_id = timeId;
    if (timeId === null) {
        time_id = $('#time_id').val();
    }
    $.post('/stop-time', {time_id})
        .done((res) => {
            if (timeId === null) {
                console.log(res);
                $('#time-' + time_id).text($('#time-' + time_id).text() + res.stoped_at);
                $('#started-at').css('display', 'none');
                $('#stop-time-' + user_id).css('display', 'none');
                $('#start-time-' + user_id).css('display', 'inline-block');
            } else {
                window.location.reload();
            }
        })
        .fail((err) => {
            showErrMsg('Error');
        })
}