let users = $('.user');
function focusOnUser() {
    let name = ($('#search-field').val()).toLowerCase();
    if (!name.trim()) return;
    for (let i = 0; i < users.length; i++) {
        if (!(users[i].classList[1].toLowerCase() === name)) {
            users[i].style.display = 'none';
        } else  {
            users[i].style.display = 'table-cell';
        }
    }
}

function clearFocus() {
    $('#search-field').val('');
    for (let j = 0; j < users.length; j++) {
        users[j].style.display = 'table-cell';
    }
}