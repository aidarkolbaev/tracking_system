function editTime(action, time_id) {
    let newVal = $('#' + action + '-' + time_id).val();
    $.post('/edit-time', {field: action, newVal, time_id: time_id})
        .done(function (res) {
            $('#success').fadeIn();
            setTimeout(() => {
                $('#success').fadeOut()
            }, 5000)
        })
        .fail(function (err) {
            if (err.status === 422) {
                showErrMsg('Invalid data')
            } else {
                showErrMsg('Error')
            }
        });
}