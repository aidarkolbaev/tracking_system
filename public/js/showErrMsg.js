function showErrMsg(msg) {
    $('#error').text(msg);
    $('#error').fadeIn();
    setTimeout(function () {
        $('#error').fadeOut();
    }, 5000)
}