<?php

use Phalcon\Mvc\Model;

class Times extends Model
{
    public static function getTimes($year = null, $month = null, $user_id = null)
    {
        $query = self::query()
            ->columns([
                'id' => 'id',
                'user_id' => 'user_id',
                'startDate' => 'DATE(start)',
                "start" => "DATE_FORMAT(start, '%H:%i')",
                "stop" => "DATE_FORMAT(stop, '%H:%i')",
                'day' => 'DAY(start)'
            ]);
        if ($user_id) {
            $query->where("DATE(start) = CURDATE() AND user_id = :user_id:", ['user_id' => $user_id]);
        } else {
            $query->where("MONTH(start) = :month: AND YEAR(start) = :year:", ['month' => $month, 'year' => $year]);
        }
        return $query->execute();
    }

    public static function getAvailableYears() {
        $result = self::query()
            ->columns([
                'years' => 'YEAR(start)'
            ])
            ->groupBy('years')
            ->execute()->toArray();
        $array = [];
        foreach ($result as $value) {
            $array[$value['years']] = $value['years'];
        }
        return $array;
    }

    public static function getTotalByDate($month = null, $year = null) {
        return self::query()
            ->columns([
                'id' => 'id',
                'user_id' => 'user_id',
                'total' =>'SUM(diff)',
                'startDate' => 'DATE(start)',
                'day' => 'DAY(start)'
            ])
            ->where('MONTH(start) = :month: AND YEAR(start) = :year:', [
                'month' => $month ?? date('m'),
                'year' => $year ?? date('Y')
                ])
            ->groupBy('startDate, user_id')
            ->orderBy('startDate, user_id')
            ->execute();
    }

    public static function getTotalByUserId($user_id, $month, $year) {
        return self::query()
            ->columns([
                'user_id' => 'user_id',
                'total' => 'SUM(diff)'
            ])
            ->where('MONTH(start) = :month: AND YEAR(start) = :year: AND user_id = :user_id:', [
                'month' => $month,
                'year' => $year,
                'user_id' => $user_id
            ])
            ->execute()->getFirst();
    }
}