<?php

use Phalcon\Mvc\Model;
use Phalcon\Validation;
use Phalcon\Validation\Validator\Email as EmailValidator;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Uniqueness as UniquenessValidator;

class Users extends Model
{
    public function validation()
    {
        $validator = new Validation();

        $validator->add(
            'username',
            new UniquenessValidator([
                'message' => 'That username is already taken'
            ]));

        $validator->add(
            'email',
            new EmailValidator([
                'message' => 'Invalid email given'
            ]));
        $validator->add(
            'email',
            new UniquenessValidator([
                'message' => 'Sorry, The email was registered by another user'
            ]));

        $validator->add(
            'name',
            new PresenceOf([
                'message' => 'Name is required'
            ]));

        $validator->add(
            'password',
            new PresenceOf([
                'message' => 'Password is required'
            ]));

        return $this->validate($validator);
    }


    public static function getLateUsers($workDayBeginning)
    {
        return self::query()
            ->columns([
               "id" => "Users.id",
               "name" => "Users.name",
               "username" => "Users.username",
               "started" => "TIME(t.start)",
            ])
            ->join("Times","Users.id = t.user_id",'t')
            ->where("DATE(t.start) = CURDATE() AND TIME(:beginning:) < (
                  SELECT MIN(t2.start)
                  FROM Times t2
                  WHERE Users.id = t2.user_id and DATE(t2.start) = CURDATE()
                )",['beginning' => $workDayBeginning])
            ->groupBy("Users.id")
            ->execute();

    }
}