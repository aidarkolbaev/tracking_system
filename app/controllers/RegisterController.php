<?php

class RegisterController extends ControllerBase
{
    public function initialize()
    {
        $this->tag->setTitle('User registration');
        $this->view->setTemplateBefore('header');
        parent::initialize();
    }

    public function indexAction()
    {
        $form = new RegisterForm;

        if ($this->request->isPost()) {
            $name = $this->request->getPost('name', 'string');
            $username = $this->request->getPost('username', 'string');
            $email = $this->request->getPost('email', 'email');
            $password = $this->request->getPost('password');
            $repeatPassword = $this->request->getPost('repeatPassword');

            if ($password != $repeatPassword) {
                $this->flash->error('Passwords are different!');
            } else {
                $user = new Users();
                $user->username = $username;
                $user->name = $name;
                $user->password = password_hash($password, PASSWORD_BCRYPT);
                $user->email = $email;
                $user->created_at = new Phalcon\Db\RawValue('now()');
                $user->role = 'user';
                $user->active = 1;
                if ($user->save()) {
                    return $this->response->redirect('/');
                } else {
                    foreach ($user->getMessages() as $message) {
                        $this->flash->error((string)$message);
                    }
                }
            }
        }
        $this->view->form = $form;
    }
}