<?php

class IndexController extends ControllerBase
{
    public function initialize()
    {
        $this->tag->setTitle('Times');
        parent::initialize();
    }

    public function indexAction()
    {
        if ($this->request->isPost()) {
            $year = $this->request->getPost('year');
            $month = $this->request->getPost('month');
            $daysInMonth = cal_days_in_month(CAL_GREGORIAN, $month, $year);
            $totals = Times::getTotalByDate($month, $year);
        } else {
            $year = date('Y');
            $month = date('n');
            $daysInMonth = date("t");
            $totals = Times::getTotalByDate();
        }
        $viewer_id = $this->session->get('auth')['id'];
        $times = Times::getTimes($year, $month);

        $users = Users::find(["role = 'user' AND active = 1 AND id <> $viewer_id"]);
        $users = $users->toArray();
        $availableYears = Times::getAvailableYears();
        $holidays = Settings::countHolidaysByYearAndMonth($year, $month);
        $nameOfDays = [];
        $numberOfWorkDays = -($holidays);

        for ($i = 1; $i <= $daysInMonth; $i++) {
            $dayName = strftime('%A', strtotime($i . '-' . $month . '-' . $year));
            $nameOfDays[$i] = $dayName;
            if (!in_array($dayName,  ['Saturday','Sunday'])) {
                $numberOfWorkDays++;
            }
        }

        if ($this->session->get('auth')['role'] != 'admin') {
            $myTime = Times::getTimes(null, null, $viewer_id)->getLast();
            $myTotal = Times::getTotalByUserId($viewer_id, $month, $year);
            $ratio = ($myTotal->total / ($numberOfWorkDays * 8 * 60 * 60)) * 100;
            $viewer = Users::findFirst([
               'conditions' => 'id = :id:',
               'bind' => ['id' => $viewer_id]
            ]);
            array_unshift($users, $viewer->toArray());
        }
        $this->view->setVars([
            'days' => $daysInMonth,
            'nameOfDays' => $nameOfDays,
            'ratio' => sprintf('%01.2f',$ratio ?? 0),
            'numberOfWorkDays' => $numberOfWorkDays,
            'totals' => $totals,
            'myTotal' => $myTotal ?? null,
            'users' => $users,
            'selected' =>  ['month' => $month, 'year' => $year],
            'times' => $times,
            'my_time' => $myTime ?? null,
            'years' => $availableYears,
            'months' => [
                1 => 'January',
                2 => 'February',
                3 => 'March',
                4 => 'April',
                5 => 'May',
                6 => 'June',
                7 => 'July',
                8 => 'August',
                9 => 'September',
                10 => 'October',
                11 => 'November',
                12 => 'December',
            ]
        ]);
    }


    public function startAction()
    {
        if ($this->request->isPost() && $this->request->isAjax()) {
            $user_id = $this->request->getPost('user_id', 'int');
            $workDayStart = Settings::findFirst(["name = 'work-day-beginning'"]);
            if ((strtotime(date('H:i:s')) + 21600) > strtotime($workDayStart->value . ':00')) {
                $time = Times::findFirst([
                    'user_id = :user_id: AND DATE(start) = CURDATE()',
                    'bind' => ['user_id' => $user_id]
                ]);
                if(!$time) {
                    $delay = new Delays();
                    $delay->created_at = new Phalcon\Db\RawValue('now()');
                    $delay->user_id = $user_id;
                    if (!$delay->save()) return false;
                }
                unset($time);
            }
            $time = new Times();
            $time->user_id = $user_id;
            $time->start = new Phalcon\Db\RawValue('now()');
            if($time->save()) {
                return $this->response->setStatusCode(201)->setJsonContent([
                    'started_at' => date('H:i', time() + 21600),
                    'time_id' => $time->id
                ]);
            }
        }
        return false;
    }

    public function stopAction()
    {
        if ($this->request->isPost() && $this->request->isAjax()) {
            $timeId = $this->request->getPost('time_id', 'int');
            if ($time = Times::findFirst([
                'id = :time_id: AND stop IS NULL',
                'bind' => ['time_id' => $timeId]
            ])) {
                $time->stop = new Phalcon\Db\RawValue('now()');

                $date = new DateTime( $time->start, new DateTimeZone('Asia/Bishkek') );
                $date2 = new DateTime('now', new DateTimeZone('Asia/Bishkek'));
                $diff = $date2->getTimestamp() - $date->getTimestamp();

                $time->diff = $diff;
                if ($time->save()) {
                    return $this->response->setStatusCode(202)->setJsonContent([
                        'diff' => $diff,
                        'stoped_at' => date('H:i', time() + 21600)
                    ]);
                }
            };
        }
        return false;
    }




    public function changePasswordAction() {
        $form = new PasswordForm();
        if($this->request->isPost()) {
            $oldPassword = $this->request->getPost('oldPassword');
            $newPassword = $this->request->getPost('password');
            $newPasswordRepeat = $this->request->getPost('repeatPassword');
            if($newPassword != $newPasswordRepeat) {
                $this->flash->error('Passwords are different!');
            } else {
                $user = Users::findFirst(['id = :id:', 'bind' => ['id' => $this->session->get('auth')['id']]]);
                if ($user && password_verify($oldPassword, $user->password)) {
                    $user->password = password_hash($newPassword, PASSWORD_BCRYPT);
                    if ($user->save()) {
                        return $this->response->redirect('/');
                    } else {
                        foreach ($user->getMessages() as $message) {
                            $this->flash->error((string)$message);
                        }
                    }
                } else {
                    $this->flash->error('Wrong Password');
                }
            }
        }
        $this->view->form = $form;
    }
}

