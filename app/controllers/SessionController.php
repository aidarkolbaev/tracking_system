<?php

class SessionController extends ControllerBase
{
    public function initialize()
    {
        $this->tag->setTitle('Sign In');
    }


    public function indexAction()
    {
        $form = new LoginForm();
        if ($this->request->isPost()) {
            if ($this->security->checkToken()) {
                $username = $this->request->getPost('username');
                $password = $this->request->getPost('password');

                $user = Users::findFirst([
                    '(username = :username: OR email = :username:) AND active = 1',
                    'bind' => ['username' => $username]
                ]);

                if ($user != false && password_verify($password, $user->password)) {
                    $this->_registerSession($user);

                    return $this->response->redirect('/');
                } else {
                    $this->flash->error('Wrong username/password');
                }
            } else {
                $this->flash->error('CSRF is not valid');
            }
        }
        $this->view->form = $form;
    }


    public function _registerSession(Users $user)
    {
        $this->session->remove('auth');
        $this->session->set('auth', [
            'id' => $user->id,
            'name' => $user->name,
            'role' => $user->role
        ]);
    }


    public function endAction()
    {
        $this->session->remove('auth');
        return $this->response->redirect('/logout');
    }
}