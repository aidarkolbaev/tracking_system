<?php

use Phalcon\Http\Response;

class AdminController extends ControllerBase
{
    public function initialize()
    {
        parent::initialize();
    }

    public function editAction() {
        $response = new Response();
        if($this->request->isAjax() && $this->request->isPost()) {
            $timeId = $this->request->getPost('time_id');
            $field = $this->request->getPost('field');
            $newVal = $this->request->getPost('newVal');
            if(preg_match('/^([01][0-9]|2[0-3]):[0-5][0-9]$/', $newVal)) {

                $time = Times::findFirst(['id = :time_id:', 'bind' => ['time_id' => $timeId]]);

                if ($field == 'start') {
                    $editingTime =& $time->start;
                    $anotherTime =& $time->stop;
                } else {
                    $editingTime =& $time->stop;
                    $anotherTime =& $time->start;
                }

                $dateObject = new DateTime($editingTime);
                $dateObject->setTime(substr($newVal, 0, 2), substr($newVal, -2, 2));

                $diff = $dateObject->getTimestamp() - (new DateTime($anotherTime))->getTimestamp();

                $time->diff = abs($diff);
                $editingTime = date_format($dateObject, 'Y-m-d H:i:s');
                if ($time->save()) {
                    $response->setStatusCode(200, 'ok');
                } else {
                    $response->setStatusCode(409);
                }
            } else {
                $response->setStatusCode(422);
            }
        }
        return $response;
    }


    public function delaysAction() {

        $beginning = Settings::findFirst(["name = 'work-day-beginning'"]);
        $dateTime = (new DateTime())->setTime(substr($beginning->value, 0, 2), substr($beginning->value, -2, 2));
        $lateUsers = Users::getLateUsers(date_format($dateTime, 'Y-m-d H:i:s'));
        $this->view->setVars([
            'beginning' => $beginning,
            'hours' => [
                '07:00' => '07:00',
                '07:30' => '07:30',
                '08:00' => '08:00',
                '08:30' => '08:30',
                '09:00' => '09:00',
                '09:30' => '09:30',
                '10:00' => '10:00',
                '10:30' => '10:30',
                '11:00' => '11:00',
                '11:30' => '11:30',
                '12:00' => '12:00',
            ],
            'lateUsers' => $lateUsers
            ]);
    }


    public function beginningAction() {
        if ($this->request->isPost()) {
            $settings = Settings::findFirst(["name = 'work-day-beginning'"]);
            if(!$settings) {
                $settings = new Settings();
                $settings->name = $this->request->getPost('name');
            }
            $settings->value = $this->request->getPost('value');
            if ($settings->save()) {
                return $this->response->redirect('/delays');
            } else {
                $this->flash->error('Error');
            }
        } else {
            return $this->flash->error('The HTTP method is not allowed');
        }
    }


    public function userAction($id) {
        $user = Users::findFirst(['id = :id:', 'bind' => ['id' => $id]]);
        $delays = Delays::find([
            'user_id = :user_id: AND MONTH(created_at) = MONTH(CURDATE())',
            'bind' => ['user_id' => $id]
        ]);
        $this->view->setVars(['user' => $user, 'delays' => $delays]);
    }


    public function deleteAction($id) {
        $user = Users::findFirst(['id = :id:', 'bind' => ['id' => $id]]);
        $user->active = 0;
        if ($user->save()) {
            $this->response->redirect('/user/' . $id);
        } else {
            $this->flash->error('Error');
        }
    }


    public function removeDelayAction($id) {
        $delay = Delays::findFirst(['id = :id:', 'bind' => ['id' => $id]]);
        $user_id = $this->request->get('user_id');
        if($delay->delete()) {
            return $this->response->redirect('/user/'.$user_id);
        };
    }


    public function holidaysAction() {
        if ($this->request->isPost()) {
            $date = $this->request->getPost('date');
            $holiday = Settings::findFirst(["name = 'holiday' AND value = :date:", 'bind' => ['date' => $date]]);
            if ($date && !$holiday) {
                $repeat = $this->request->getPost('repeat');
                $holiday = new Settings();
                $holiday->name = 'holiday';
                $holiday->value = $date;
                $holiday->bool = $repeat ? 1 : 0;
                if ($holiday->save()) {
                    return $this->response->redirect('/holidays');
                }
            }
        } else {
            $holidays = Settings::find(["name = 'holiday'"]);
            $this->view->setVars(['holidays' => $holidays]);
        }
    }
}