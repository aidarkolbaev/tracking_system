<?php

use Phalcon\Forms\Element\Password;
use Phalcon\Forms\Form;

class PasswordForm extends Form
{
    public function initialize($entity = null, $options = null)
    {
        $oldPassword = new Password('oldPassword');
        $oldPassword->setLabel('Old Password');
        $this->add($oldPassword);

        $password = new Password('password');
        $password->setLabel('New Password');
        $this->add($password);

        $repeatPassword = new Password('repeatPassword');
        $repeatPassword->setLabel('Repeat New Password');
        $this->add($repeatPassword);
    }
}