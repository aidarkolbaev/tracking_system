<?php

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Password;

class RegisterForm extends Form
{
    public function initialize($entity = null, $options = null)
    {
        $name = new Text('name');
        $name->setLabel('User Full Name');
        $name->setFilters('string');
        $this->add($name);

        $username = new Text('username');
        $username->setLabel('Username');
        $username->setFilters('string');
        $this->add($username);


        $email = new Text('email');
        $email->setLabel('Email');
        $email->setFilters('email');
        $this->add($email);

        $password = new Password('password');
        $password->setLabel('Password');
        $this->add($password);

        $repeatPassword = new Password('repeatPassword');
        $repeatPassword->setLabel('Repeat Password');
        $this->add($repeatPassword);
     }
}