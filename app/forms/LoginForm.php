<?php

use Phalcon\Forms\Element\Password;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Form;

class LoginForm extends Form
{
    public function initialize($entity = null, $options = null)
    {

        $username = new Text('username');
        $username->setLabel('Username:');
        $this->add($username);

        $password = new Password('password');
        $password->setLabel('Password:');
        $this->add($password);
    }
}