
{{ content() }}

<div class="jumbotron", style="text-align: center">
    <h1>Forbidden</h1>
    <p>You don't have access to this option. Contact an administrator</p>
    <p>{{ link_to('/', 'Home', 'class': 'btn btn-primary') }}</p>
</div>