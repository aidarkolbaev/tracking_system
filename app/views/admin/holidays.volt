{{ content() }}
<div class="holidays">
    <hr>
    <div style="width: 600px; margin: 20px auto">
        <h2 style="text-align: center">Add holiday:</h2>
        <div style="text-align: center">
            {{ form('/holidays', 'method' : 'post') }}
                <label style="margin-right: 20px" for="holiday">Select date:</label>
                <input type="date" id="holiday" name="date">
                <label><input type="checkbox" name="repeat">repeat</label>
                <div style="text-align: center; margin: 20px">
                    <input type="submit" class="btn btn-primary" value="Add">
                </div>
            </form>
        </div>
    </div>
    <hr>
    <div class="holidays-list">
        <h2 style="text-align: center">Holidays:</h2>
        {% for holiday in holidays %}
            <div class="holiday">
                {{ holiday.value }}
                {% if holiday.bool %}
                    <div class="repeat">every year</div>
                {% endif %}
            </div>
        {% endfor %}
    </div>
</div>


<style>
    .holiday {
        text-align: center;
        width: 300px;
        margin: 10px auto;
        border-bottom: 1px solid lightgray;
        padding: 5px;
        font-size: 18px;
        font-weight: 600;
        color: #057494;
        position: relative;
    }
    .repeat {
        display: inline-block;
        padding: 5px;
        background: #89d388;
        font-size: 12px;
        left: 5px;
        color: #00562b;
        border-radius: 5px;
        position: absolute;
    }

</style>