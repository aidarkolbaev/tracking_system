{{ content() }}

<div class="delays">
    <h2 style="text-align: center">Delays</h2>
    <hr>
    {% if beginning %}
        <h3 style="text-align: center">The work day start at <span style="font-weight: bold">{{ beginning.value }}</span> o'clock</h3>
        <hr>
    {% endif %}
    {{ form('/work-day-beginning', 'method' : 'post') }}
        <div style="text-align: center"><label for="work-hours" style="font-size: 16px; margin: 10px 0">Change the beginning of the work day:</label></div>
        <div style="text-align: center">
            <input type="hidden" name="name" value="work-day-beginning">
            {{ select('value', hours, 'using' : ['id', 'name']) }}
            <input style="margin: 20px" type="submit" value="Change" class="btn btn-primary">
        </div>
    </form>
    <hr>
    <h2 style="text-align: center">Users, who late today:</h2>
    {% for user in lateUsers %}
        <div class="late-user"><div class="index">{{ loop.index }}.</div>{{ link_to('/user/'~user.id, user.name) }} came at <div class="started-time">{{ user.started }}</div></div>
    {% else %}
       <div style="text-align: center; font-weight: bold; margin: 20px">No one was late</div>
    {% endfor %}
</div>


<style>
    .late-user {
        padding: 15px;
        background: white;
        box-shadow: 0 2px 10px #b9b9b9;
        font-size: 16px;
        margin: 10px auto;
        width: 600px;
    }
    .started-time {
        font-weight: bold;
        display: inline-block;
        color: darkred;
    }
    .index {
        margin: 0 10px;
        display: inline-block;
        font-weight: bold;
    }
</style>