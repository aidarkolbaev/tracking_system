{{ content() }}
<div class="user-info">
    <div style="text-align: right">
        {% if user.active == 1 %}
            {{ form('/delete/user/'~user.id) }}
                <button type="submit" class="btn btn-danger" onclick="this.form.submit()">Delete User</button>
            </form>
        {% else %}
            <div class="deleted">DELETED</div>
        {% endif %}
    </div>
    <h2>User info:</h2>
    <div class="info">Full name: <span class="bold">{{ user.name }}</span></div>
    <div class="info">Username: <span class="bold">{{ user.username }}</span></div>
    <div class="info">Email: <span class="bold">{{ user.email }}</span></div>
</div>
<div class="user-delays">
    <h3 style="text-align: center; color: darkred; margin: 20px">{{ user.name }} was late {{ delays|length }} times in current month:</h3>
    {% for delay in delays %}
        <div style="position: relative">
            <div style="margin: 5px; color: darkred">{{ loop.index }}) {{ delay.created_at }}</div>
            <div style="position: absolute; right: 20px; top: 0">
                {{ form('/delete/delay/'~delay.id) }}
                    <input type="submit" class="btn btn-danger" value="Delete">
                    <input type="hidden" name="user_id" value="{{ delay.user_id }}">
                </form>
            </div>
        </div>
        <hr>
    {% else %}
        <hr>
        <div style="text-align: center; color: darkgreen">Good!</div>
        <hr>
    {% endfor %}
</div>

<style>
    .user-info {
        width: 800px;
        margin: 20px auto;
        padding: 20px;
        background: #f0f0f0;
        border-radius: 5px;
    }
    .info {
        margin: 20px 0;
        font-weight: 500;
        font-size: 20px;
    }
    .bold {
        font-weight: bold;
        margin: 0 10px;
    }
    .deleted {
        background: rgba(158, 0, 0, 0.29);
        font-size: 22px;
        color: darkred;
        font-weight: bold;
        text-align: center;
        padding: 5px;
    }
    .user-delays {
        width: 800px;
        margin: 20px auto;
        padding: 20px;
    }
</style>