{{ stylesheet_link('css/header.css') }}
<div class="header">
    <div class="header__container">
        {{ link_to('/', 'Table') }}
        {% if session.get('auth')['role'] == 'admin' %}
            {{ link_to('/registration', 'Register user') }}
            {{ link_to('/delays', 'Delays') }}
            {{ link_to('/holidays', 'Holidays') }}
        {% else %}
            {{ link_to('/change-password', 'Change Password') }}
        {% endif %}
        {{ link_to('/logout', 'Logout') }}
    </div>
</div>
{{ flash.output() }}
<div class="container">
    {{ content() }}
</div>

{% if session.get('auth')['role'] == 'admin' %}
    <script>
        let header = document.body.getElementsByClassName('header');
        header[0].style.transition = 'all .5s';
        header[0].style.background = '#27afa1';
    </script>
{% endif %}
