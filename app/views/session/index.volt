{{ content() }}
<div class="session">
    <div class="session__title">
        <h2>Authentication:</h2>
    </div>
    <div class="session__fields">
        {{ form('/sign-in', 'role': 'form', 'method' : 'post') }}
            <input type="hidden" name="{{ security.getTokenKey() }}" value="{{ security.getToken() }}">
            {{ form.label('username') }}
            <div style="margin: 5px 0 20px">
                {{ form.render('username', ['class': 'form-control']) }}
            </div>
            {{ form.label('password') }}
            <div style="margin: 5px 0 20px">
                {{ form.render('password', ['class': 'form-control']) }}
            </div>
            <div>
                {{ submit_button('Login', ['class' : 'btn btn-primary']) }}
            </div>
        </form>
    </div>
</div>