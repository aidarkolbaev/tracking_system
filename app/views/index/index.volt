{{ content() }}
{{ stylesheet_link('css/table.css') }}
{% set auth =  session.get('auth') %}
<hr>
<div>
    {% if auth['role'] != 'admin' %}
        <h1>My hours log</h1>
        <div>You have:
            <div class="bold seconds-hh-mm">{{ myTotal.total }}</div>
        </div>
        <div>You have/Assigned:
            <div class="bold">{{ ratio }}%</div>
        </div>
        <div>Assigned:
            <div class="bold">{{ numberOfWorkDays * 8 }}</div>
        </div>
        <div class="start-stop-wrapper">
            {% if my_time and my_time.stop or my_time == false %}
                <button class="btn btn-primary btn-lg" style="display: none" id="stop-time-{{ auth['id'] }}"
                        onclick="stopTime({{ auth['id'] }})">Stop
                </button>
                <button class="btn btn-primary btn-lg" id="start-time-{{ auth['id'] }}"
                        onclick="startTime({{ auth['id'] }})">Start
                </button>
                <input type="hidden" id="time_id" value="{{ my_time.id }}">
                <br>
                <div id="started-at" class="bold" style="display: none">started at: {{ my_time.start }}</div>
            {% else %}
                <button class="btn btn-primary btn-lg" id="stop-time-{{ auth['id'] }}"
                        onclick="stopTime({{ auth['id'] }})">Stop
                </button>
                <button class="btn btn-primary btn-lg" style="display: none" id="start-time-{{ auth['id'] }}"
                        onclick="startTime({{ auth['id'] }})">Start
                </button>
                <input type="hidden" id="time_id" value="{{ my_time.id }}">
                <br>
                <div id="started-at" class="bold">started at: {{ my_time.start }}</div>
            {% endif %}
        </div>
    {% else %}
        <h1 style="text-align: center">Admin panel:</h1>
    {% endif %}
</div>
<hr>
<div>
    <h1 style="display: inline-block">Table:</h1>
    <div style="display: inline-block; margin-left: 50px">
        {{ form('/', 'method' : 'post') }}
        {{ select("year", years, 'value' : selected['year'], 'using' : ['id', 'name']) }}
        {{ select("month", months, 'value' : selected['month'], 'using' : ['id', 'name']) }}
        </form>
    </div>
    {% if auth['role'] == 'admin' %}
        <div style="display: inline-block; margin-left: 200px">
            <label for="search-field">Search user:</label>
            <div>
                <input type="text" id="search-field" onkeyup="focusOnUser()"
                       placeholder="Enter the name of the user...">
                <button class="btn btn-info btn-sm" id="search" onclick="focusOnUser()">search</button>
                <div class="clear-button" onclick="clearFocus()">clear</div>
            </div>
        </div>
    {% endif %}
</div>

<div id="error" style="display: none">Invalid data</div>
<div id="success" style="display: none">Successfully updated</div>

<div class="table_wrapper" style="margin: 10px">
    <table class="table table-bordered">
        <tr>
            <th style="text-decoration: underline; color: darkgreen; cursor: pointer" onclick="showHide()">Hide/show
            </th>
            {% for user in users %}
                <th scope="col" style="text-align: center" class="user {{ user['name'] }} {{ user['id'] }}">
                    {% if auth['role'] == 'admin' %}
                        {{ link_to('/user/'~user['id'] , user['name']) }}
                    {% else %}
                        {{ user['name'] }}
                    {% endif %}
                </th>
            {% endfor %}
        </tr>
        {% for day in 1..days %}
            <tr class="day {% if nameOfDays[day] in ['Saturday', 'Sunday'] %}day-off{% endif %}">
                <td>
                    <div style="font-size: 18px">{{ day }}</div>
                    <div class="name-of-day">{{ nameOfDays[day] }}</div>
                </td>
                {% for user in users %}
                    <td class="user {{ user['name'] }} {{ user['id'] }}">
                        {% for  time in times %}
                            {% if time.user_id == user['id'] and day == time.day %}
                                {% if auth['role'] == 'admin' %}
                                    <div style="white-space: nowrap">
                                        <input type="text" maxlength="5" id="start-{{ time.id }}"
                                               onchange="editTime('start', {{ time.id }})"
                                               style="width: 60px; text-align: center; margin-top: 5px"
                                               value="{{ time.start }}"> -
                                        <input type="text" maxlength="5" id="stop-{{ time.id }}"
                                               onchange="editTime('stop', {{ time.id }})"
                                               style="width: 60px; text-align: center" value="{{ time.stop }}">
                                    </div>
                                    {% if loop.last and time.day == day %}
                                        {% if time.stop %}
                                            <div>
                                                <button type="submit" class="btn btn-primary btn-sm" style="margin: 10px 0"
                                                        onclick="adminStartTime({{ user['id'] }})">Start
                                                </button>
                                            </div>
                                        {% else %}
                                            {{ form('/stop-time', 'method' : 'post') }}
                                            <button type="submit" class="btn btn-primary btn-sm" style="margin: 10px 0"
                                                    onclick="stopTime({{ user['id'] }}, {{ time.id }})">Stop
                                            </button>
                                            </form>
                                        {% endif %}
                                    {% endif %}
                                {% else %}
                                    <div style="white-space: nowrap" id="time-{{ time.id }}">{{ time.start }} - {{ time.stop }}</div>
                                {% endif %}
                            {% else %}
                                {% if loop.last and day == date('d') and auth['role'] == 'admin' %}
                                    <div>
                                        <button type="submit" class="btn btn-primary btn-sm" style="margin: 10px 0"
                                                onclick="adminStartTime({{ user['id'] }})">Start
                                        </button>
                                    </div>
                                {% endif %}
                            {% endif %}
                        {% endfor %}
                        {% for total in totals %}
                            {% if total.day == day and user['id'] == total.user_id %}
                                {% if total.total < 28800 and total.startDate != date('Y-m-d') %}
                                    <div class="bold" id="user-{{ user['id'] }}-day-{{ day }}-total" style="color: red; display: block">total:
                                        <div class="seconds-hh-mm">{{ total.total }}</div>
                                    </div>
                                    <div class="bold" style="color: red; display: block">less:
                                        <div class="seconds-hh-mm">{{ 28800 - total.total }}</div>
                                    </div>
                                {% else %}
                                    <div class="bold" id="user-{{ user['id'] }}-day-{{ day }}-total" style="color: darkgreen">total:
                                        <div class="seconds-hh-mm">{{ total.total }}</div>
                                    </div>
                                {% endif %}
                            {% endif %}
                        {% endfor %}

                    </td>
                {% endfor %}
            </tr>
        {% endfor %}
    </table>
</div>

<style>
    #error {
        position: fixed;
        font-weight: 600;
        padding: 10px;
        border: 1px solid red;
        border-radius: 5px;
        background: rgba(255, 0, 0, 0.21);
        color: darkred;
        right: 20px;
        top: 100px;
    }

    #success {
        position: fixed;
        right: 20px;
        top: 200px;
        color: darkgreen;
        font-weight: 500;
        padding: 10px;
        border: 1px solid green;
        border-radius: 5px;
        background: rgba(144, 238, 144, 0.5);
    }

    .name-of-day {
        font-size: 12px;
        padding: 2px 5px;
        border: 1px solid lightgray;
        background: white;
        margin: 5px;
        display: inline-block;
    }

    .day-off {
        background: rgba(0, 255, 14, 0.11);
    }

    .bold {
        display: inline-block;
        font-weight: bold;
    }

    .seconds-hh-mm {
        display: inline-block;
    }

    #search-field {
        width: 250px;
        padding-right: 20px;
    }

    .clear-button {
        font-size: 12px;
        color: darkred;
        cursor: pointer;
    }

    .clear-button:hover {
        text-decoration: underline;
    }

    .start-stop-wrapper {
        text-align: center;
        margin: 20px;
    }
</style>


<script>
</script>