{{ content() }}
<div class="registration">
    <div class="registration__title">
        <h2>User Registration</h2>
    </div>
    {{ form('register', 'id' : 'registerForm', 'role' : 'form') }}
        <div class="control-group">
            {{ form.label('name', ['class': 'control-label']) }}
            <div class="controls">
                {{ form.render('name', ['class': 'form-control']) }}
            </div>
        </div>
        <div class="control-group">
            {{ form.label('username', ['class': 'control-label']) }}
            <div class="controls">
                {{ form.render('username', ['class': 'form-control']) }}
            </div>
        </div>
        <div class="control-group">
            {{ form.label('email', ['class': 'control-label']) }}
            <div class="controls">
                {{ form.render('email', ['class': 'form-control']) }}
            </div>
        </div>
        <div class="control-group">
            {{ form.label('password', ['class': 'control-label']) }}
            <div class="controls">
                {{ form.render('password', ['class': 'form-control']) }}
            </div>
        </div>
        <div class="control-group">
            {{ form.label('repeatPassword', ['class': 'control-label']) }}
            <div class="controls">
                {{ form.render('repeatPassword', ['class': 'form-control']) }}
            </div>
        </div>
        {{ submit_button('Register', 'class': 'btn btn-primary', 'onclick': 'return SignUp.validate();') }}
    </form>
</div>