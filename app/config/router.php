<?php

$router = $di->getRouter();

// Define your routes here
$router->add('/login', [
    'controller' => 'session',
    'action' => 'index'
]);
$router->add('/registration', [
    'controller' => 'register',
    'action' => 'index'
]);
$router->add('/logout', [
    'controller' => 'session',
    'action' => 'end'
]);
$router->add('/start-time', [
    'controller' => 'index',
    'action' => 'start'
]);
$router->add('/stop-time', [
    'controller' => 'index',
    'action' => 'stop'
]);
$router->add('/change-password', [
    'controller' => 'index',
    'action' => 'changePassword'
]);
$router->add('/edit-time', [
    'controller' => 'admin',
    'action' => 'edit'
]);
$router->add('/delays', [
    'controller' => 'admin',
    'action' => 'delays'
]);
$router->add('/work-day-beginning', [
    'controller' => 'admin',
    'action' => 'beginning'
]);
$router->add('/user/{id}', [
    'controller' => 'admin',
    'action' => 'user'
]);
$router->add('/delete/user/{id}', [
    'controller' => 'admin',
    'action' => 'delete'
]);
$router->add('/sign-in', [
    'controller' => 'session',
    'action' => 'index'
]);
$router->add('/delete/delay/{id}', [
    'controller' => 'admin',
    'action' => 'removeDelay'
]);
$router->add('/holidays', [
    'controller' => 'admin',
    'action' => 'holidays'
]);
$router->handle();
