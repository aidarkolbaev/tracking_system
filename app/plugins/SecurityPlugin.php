<?php

use Phalcon\Acl;
use Phalcon\Acl\Role;
use Phalcon\Acl\Resource;
use Phalcon\Events\Event;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Mvc\User\Plugin;
use Phalcon\Acl\Adapter\Memory as AclList;

class SecurityPlugin extends Plugin
{
    public function getAcl()
    {
        if (empty($this->persistent->acl)) {
            $acl = new AclList();

            $acl->setDefaultAction(Acl::DENY);

            $roles = [
                'user' => new Role('User'),
                'guest' => new Role('Guest'),
                'admin' => new Role('Admin')
            ];
            foreach ($roles as $role) {
                $acl->addRole($role);
            }


            $adminResources = [
                'register' => ['index'],
                'admin' => ['edit', 'delays', 'beginning', 'user', 'delete', 'removeDelay', 'holidays']
            ];
            foreach ($adminResources as $resource => $actions) {
                $acl->addResource(new Resource($resource), $actions);
            }


            $userResources = [
                'index' => ['index', 'start', 'stop', 'changePassword'],
                'session' => ['end']
            ];
            foreach ($userResources as $resource => $actions) {
                $acl->addResource(new Resource($resource), $actions);
            }

            $publicResources = [
                'session' => ['index', 'start'],
                'errors' => ['show401', 'show404', 'show403'],
            ];

            // Доступ к публичным ресурсам всем
            foreach ($publicResources as $resource => $actions) {
                $acl->addResource(new Resource($resource), $actions);
            }

            // Доступ к административной части только админу
            foreach ($adminResources as $resource => $actions) {
                foreach ($actions as $action) {
                    $acl->allow('Admin', $resource, $action);
                }
            }

            foreach ($roles as $role) {
                // Доступ к ресурсам пользователя админу и пользователю
                if ($role->getName() != 'Guest') {
                    foreach ($userResources as $resource => $actions) {
                        foreach ($actions as $action) {
                            $acl->allow($role->getName(), $resource, $action);
                        }
                    }
                }
                foreach ($publicResources as $resource => $actions) {
                    foreach ($actions as $action) {
                        $acl->allow($role->getName(), $resource, $action);
                    }
                }
            }


            $this->persistent->acl = $acl;
        }

        return $this->persistent->acl;
    }


    public function beforeExecuteRoute(Event $event, Dispatcher $dispatcher)
    {
        $auth = $this->session->get('auth');
        if (!$auth) {
            $role = 'Guest';
        } else if ($auth['role'] == 'admin') {
            $role = 'Admin';
        } else {
            $role = 'User';
        }

        $controller = $dispatcher->getControllerName();
        $action = $dispatcher->getActionName();


        $acl = $this->getAcl();

        if (!$acl->isResource($controller)) {
            $dispatcher->forward([
                'controller' => 'errors',
                'action' => 'show404'
            ]);
            return false;
        }

        $allowed = $acl->isAllowed($role, $controller, $action);

        if (!$allowed) {
            if ($role === 'User') {
                $dispatcher->forward([
                    'controller' => 'errors',
                    'action' => 'show403'
                ]);
            } else {
                $this->session->destroy();
                $this->response->redirect('/login');
            }
            return false;
        }
    }
}