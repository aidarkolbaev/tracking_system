<?= $this->tag->stylesheetLink('css/admin.css') ?>
<div class="admin__header">
    <div class="admin__header__container">
        <?= $this->tag->linkTo(['/', 'Main menu']) ?>
        <?= $this->tag->linkTo(['/registration', 'Register user']) ?>
        <?= $this->tag->linkTo(['/logout', 'Logout']) ?>
    </div>
</div>
<?= $this->flash->output() ?>
<?= $this->getContent() ?>
