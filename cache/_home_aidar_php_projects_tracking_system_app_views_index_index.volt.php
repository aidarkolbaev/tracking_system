<?= $this->getContent() ?>
<?= $this->tag->stylesheetLink('css/table.css') ?>
<?php $auth = $this->session->get('auth'); ?>
<hr>
<div>
    <?php if ($auth['role'] != 'admin') { ?>
        <h1>My hours log</h1>
        <div>You have: <div class="bold seconds-hh-mm"><?= $myTotal->total ?></div></div>
        <div>You have/Assigned: <div class="bold"><?= $ratio ?>%</div></div>
        <div>Assigned: <div class="bold"><?= $numberOfWorkDays * 8 ?></div></div>
        <?php $v169840159176708172491iterated = false; ?><?php $v169840159176708172491iterator = $my_times; $v169840159176708172491incr = 0; $v169840159176708172491loop = new stdClass(); $v169840159176708172491loop->self = &$v169840159176708172491loop; $v169840159176708172491loop->length = count($v169840159176708172491iterator); $v169840159176708172491loop->index = 1; $v169840159176708172491loop->index0 = 1; $v169840159176708172491loop->revindex = $v169840159176708172491loop->length; $v169840159176708172491loop->revindex0 = $v169840159176708172491loop->length - 1; ?><?php foreach ($v169840159176708172491iterator as $time) { ?><?php $v169840159176708172491loop->first = ($v169840159176708172491incr == 0); $v169840159176708172491loop->index = $v169840159176708172491incr + 1; $v169840159176708172491loop->index0 = $v169840159176708172491incr; $v169840159176708172491loop->revindex = $v169840159176708172491loop->length - $v169840159176708172491incr; $v169840159176708172491loop->revindex0 = $v169840159176708172491loop->length - ($v169840159176708172491incr + 1); $v169840159176708172491loop->last = ($v169840159176708172491incr == ($v169840159176708172491loop->length - 1)); ?><?php $v169840159176708172491iterated = true; ?>
            <?php if ($v169840159176708172491loop->last) { ?>
                <?php if ($time->stop == null) { ?>
                    <div style="text-align: center">
                        <div style="font-size: 18px;font-weight: 600">Started at: <?= $time->start ?></div>
                        <?= $this->tag->form(['/stop-time', 'method' => 'post']) ?>
                            <button type="submit" class="btn btn-primary btn-lg" name="time_id" value="<?= $time->id ?>">Stop</button>
                        </form>
                    </div>
                <?php } else { ?>
                    <div style="text-align: center">
                        <div style="color: darkred"><?= $time->diff ?></div>
                        <?= $this->tag->form(['/start-time', 'method' => 'post']) ?>
                            <button type="submit" class="btn btn-primary btn-lg" name="user_id" value="<?= $auth['id'] ?>">Start</button>
                        </form>
                    <?php foreach ($totals as $total) { ?>
                        <?php if ($total->user_id == $auth['id'] && $total->startDate == date('Y-m-d')) { ?>
                            <div style="margin: 10px">Today Total: <div class="bold seconds-hh-mm"> <?= $total->total ?></div></div>
                        <?php } ?>
                    <?php } ?>
                    </div>
                <?php } ?>
            <?php } ?>
        <?php $v169840159176708172491incr++; } if (!$v169840159176708172491iterated) { ?>
            <div style="text-align: center">
                <?= $this->tag->form(['/start-time', 'method' => 'post']) ?>
                    
                        
                        
                    
                     <button type="submit" class="btn btn-primary btn-lg" name="user_id" value="<?= $auth['id'] ?>">Start</button>
                </form>
            </div>
        <?php } ?>
    <?php } else { ?>
        <h1 style="text-align: center">Admin panel:</h1>
    <?php } ?>
</div>
<hr>
<div>
    <h1 style="display: inline-block">Table:</h1>
    <div style="display: inline-block; margin-left: 50px">
        <?= $this->tag->form(['/', 'method' => 'post']) ?>
            <?= $this->tag->select(['year', $years, 'value' => $selected['year'], 'using' => ['id', 'name']]) ?>
            <?= $this->tag->select(['month', $months, 'value' => $selected['month'], 'using' => ['id', 'name']]) ?>
        </form>
    </div>
</div>

<div id="error" style="display: none">Данные не валидны</div>
<div id="success" style="display: none">Изменения успешно сохранены</div>

<div class="table_wrapper" style="margin: 10px">
    <table class="table table-bordered">
        <tr>
            <th style="text-decoration: underline; color: darkgreen; cursor: pointer" onclick="showHide()">Hide/show</th>
            <?php $v169840159176708172491iterator = $users; $v169840159176708172491incr = 0; $v169840159176708172491loop = new stdClass(); $v169840159176708172491loop->self = &$v169840159176708172491loop; $v169840159176708172491loop->length = count($v169840159176708172491iterator); $v169840159176708172491loop->index = 1; $v169840159176708172491loop->index0 = 1; $v169840159176708172491loop->revindex = $v169840159176708172491loop->length; $v169840159176708172491loop->revindex0 = $v169840159176708172491loop->length - 1; ?><?php foreach ($v169840159176708172491iterator as $user) { ?><?php $v169840159176708172491loop->first = ($v169840159176708172491incr == 0); $v169840159176708172491loop->index = $v169840159176708172491incr + 1; $v169840159176708172491loop->index0 = $v169840159176708172491incr; $v169840159176708172491loop->revindex = $v169840159176708172491loop->length - $v169840159176708172491incr; $v169840159176708172491loop->revindex0 = $v169840159176708172491loop->length - ($v169840159176708172491incr + 1); $v169840159176708172491loop->last = ($v169840159176708172491incr == ($v169840159176708172491loop->length - 1)); ?>
                    <th scope="col" style="text-align: center">
                        <?php if ($auth['role'] == 'admin') { ?>
                            <?= $this->tag->linkTo(['/user/' . $user['id'], $user['name']]) ?>
                        <?php } else { ?>
                            <?= $user['name'] ?>
                        <?php } ?>
                    </th>
            <?php $v169840159176708172491incr++; } ?>
        </tr>
        <?php $v169840159176708172491iterator = range(1, $days); $v169840159176708172491incr = 0; $v169840159176708172491loop = new stdClass(); $v169840159176708172491loop->self = &$v169840159176708172491loop; $v169840159176708172491loop->length = count($v169840159176708172491iterator); $v169840159176708172491loop->index = 1; $v169840159176708172491loop->index0 = 1; $v169840159176708172491loop->revindex = $v169840159176708172491loop->length; $v169840159176708172491loop->revindex0 = $v169840159176708172491loop->length - 1; ?><?php foreach ($v169840159176708172491iterator as $day) { ?><?php $v169840159176708172491loop->first = ($v169840159176708172491incr == 0); $v169840159176708172491loop->index = $v169840159176708172491incr + 1; $v169840159176708172491loop->index0 = $v169840159176708172491incr; $v169840159176708172491loop->revindex = $v169840159176708172491loop->length - $v169840159176708172491incr; $v169840159176708172491loop->revindex0 = $v169840159176708172491loop->length - ($v169840159176708172491incr + 1); $v169840159176708172491loop->last = ($v169840159176708172491incr == ($v169840159176708172491loop->length - 1)); ?>
            <tr class="day <?php if ($this->isIncluded($nameOfDays[$day], ['Saturday', 'Sunday'])) { ?>day-off<?php } ?>">
                <td>
                    <div style="font-size: 18px"><?= $day ?></div>
                    <div class="name-of-day"><?= $nameOfDays[$day] ?></div>
                </td>
                <?php foreach ($users as $user) { ?>
                        <td>
                            <?php $v169840159176708172493iterator = $times; $v169840159176708172493incr = 0; $v169840159176708172493loop = new stdClass(); $v169840159176708172493loop->self = &$v169840159176708172493loop; $v169840159176708172493loop->length = count($v169840159176708172493iterator); $v169840159176708172493loop->index = 1; $v169840159176708172493loop->index0 = 1; $v169840159176708172493loop->revindex = $v169840159176708172493loop->length; $v169840159176708172493loop->revindex0 = $v169840159176708172493loop->length - 1; ?><?php foreach ($v169840159176708172493iterator as $time) { ?><?php $v169840159176708172493loop->first = ($v169840159176708172493incr == 0); $v169840159176708172493loop->index = $v169840159176708172493incr + 1; $v169840159176708172493loop->index0 = $v169840159176708172493incr; $v169840159176708172493loop->revindex = $v169840159176708172493loop->length - $v169840159176708172493incr; $v169840159176708172493loop->revindex0 = $v169840159176708172493loop->length - ($v169840159176708172493incr + 1); $v169840159176708172493loop->last = ($v169840159176708172493incr == ($v169840159176708172493loop->length - 1)); ?>
                                <?php if ($time->user_id == $user['id'] && $day == $time->day) { ?>
                                    <?php if ($auth['role'] == 'admin') { ?>
                                        <div style="white-space: nowrap">
                                            <input type="text" maxlength="5" id="start-<?= $time->id ?>"
                                                   onchange="editTime('start', <?= $time->id ?>)"
                                                   style="width: 60px; text-align: center; margin-top: 5px"
                                                   value="<?= $time->start ?>"> -
                                            <input type="text" maxlength="5" id="stop-<?= $time->id ?>"
                                                   onchange="editTime('stop', <?= $time->id ?>)"
                                                   style="width: 60px; text-align: center" value="<?= $time->stop ?>">
                                        </div>
                                        <?php if ($v169840159176708172493loop->last) { ?>
                                            <?php if ($time->stop) { ?>
                                                <?= $this->tag->form(['/start-time', 'method' => 'post']) ?>
                                                    <button type="submit" class="btn btn-primary btn-sm" style="margin: 10px 0" name="user_id" value="<?= $user['id'] ?>">Start</button>
                                                </form>
                                            <?php } else { ?>
                                                <?= $this->tag->form(['/stop-time', 'method' => 'post']) ?>
                                                    <button type="submit" class="btn btn-primary btn-sm" style="margin: 10px 0" name="time_id" value="<?= $time->id ?>">Stop</button>
                                                </form>
                                            <?php } ?>
                                        <?php } ?>
                                    <?php } else { ?>
                                        <div style="white-space: nowrap"><?= $time->start ?> - <?= $time->stop ?></div>
                                    <?php } ?>
                                <?php } elseif ($v169840159176708172493loop->last && $day == date('d') && $auth['role'] == 'admin') { ?>
                                    <?= $this->tag->form(['/start-time', 'method' => 'post']) ?>
                                        <button type="submit" class="btn btn-primary btn-sm" style="margin: 10px 0" name="user_id" value="<?= $user['id'] ?>">Start</button>
                                    </form>
                                <?php } ?>
                            <?php $v169840159176708172493incr++; } ?>
                            <?php $v169840159176708172493iterator = $totals; $v169840159176708172493incr = 0; $v169840159176708172493loop = new stdClass(); $v169840159176708172493loop->self = &$v169840159176708172493loop; $v169840159176708172493loop->length = count($v169840159176708172493iterator); $v169840159176708172493loop->index = 1; $v169840159176708172493loop->index0 = 1; $v169840159176708172493loop->revindex = $v169840159176708172493loop->length; $v169840159176708172493loop->revindex0 = $v169840159176708172493loop->length - 1; ?><?php foreach ($v169840159176708172493iterator as $total) { ?><?php $v169840159176708172493loop->first = ($v169840159176708172493incr == 0); $v169840159176708172493loop->index = $v169840159176708172493incr + 1; $v169840159176708172493loop->index0 = $v169840159176708172493incr; $v169840159176708172493loop->revindex = $v169840159176708172493loop->length - $v169840159176708172493incr; $v169840159176708172493loop->revindex0 = $v169840159176708172493loop->length - ($v169840159176708172493incr + 1); $v169840159176708172493loop->last = ($v169840159176708172493incr == ($v169840159176708172493loop->length - 1)); ?>
                                <?php if ($total->day == $day && $user['id'] == $total->user_id) { ?>
                                    <?php if ($total->total < 28800 && $total->startDate != date('Y-m-d')) { ?>
                                        <div class="bold" style="color: red; display: block">
                                            total: <div class="seconds-hh-mm"><?= $total->total ?></div>
                                        </div>
                                        <div class="bold" style="color: red; display: block">
                                            less: <div class="seconds-hh-mm"><?= 28800 - $total->total ?></div>
                                        </div>
                                    <?php } else { ?>
                                        <div class="bold" style="color: darkgreen">
                                            total: <div class="seconds-hh-mm"><?= $total->total ?></div>
                                        </div>
                                    <?php } ?>
                                <?php } ?>
                            <?php $v169840159176708172493incr++; } ?>

                        </td>
                <?php } ?>
            </tr>
        <?php $v169840159176708172491incr++; } ?>
    </table>
</div>

<style>
    #error {
        position: fixed;
        font-weight: 600;
        padding: 10px;
        border: 1px solid red;
        border-radius: 5px;
        background: rgba(255, 0, 0, 0.21);
        color: darkred;
        right: 20px;
        top: 100px;
    }

    #success {
        position: fixed;
        right: 20px;
        top: 200px;
        color: darkgreen;
        font-weight: 500;
        padding: 10px;
        border: 1px solid green;
        border-radius: 5px;
        background: rgba(144, 238, 144, 0.5);
    }
    .name-of-day {
        font-size: 12px;
        padding: 2px 5px;
        border: 1px solid lightgray;
        background: white;
        margin: 5px;
        display: inline-block;
    }
    .day-off {
        background: rgba(0, 255, 14, 0.11);
    }
    .bold {
        display: inline-block;
        font-weight: bold;
    }
    .seconds-hh-mm {
        display: inline-block;
    }
</style>