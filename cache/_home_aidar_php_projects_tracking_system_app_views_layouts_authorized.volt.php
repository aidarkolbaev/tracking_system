<?= $this->tag->stylesheetLink('css/header.css') ?>
<div class="header">
    <div class="header__container">
        <?= $this->tag->linkTo(['/', 'Table']) ?>
        <?php if ($this->session->get('auth')['role'] == 'admin') { ?>
            <?= $this->tag->linkTo(['/registration', 'Register user']) ?>
            <?= $this->tag->linkTo(['/delays', 'Delays']) ?>
            <?= $this->tag->linkTo(['/holidays', 'Holidays']) ?>
        <?php } else { ?>
            <?= $this->tag->linkTo(['/change-password', 'Change Password']) ?>
        <?php } ?>
        <?= $this->tag->linkTo(['/logout', 'Logout']) ?>
    </div>
</div>
<?= $this->flash->output() ?>
<div class="container">
    <?= $this->getContent() ?>
</div>

<?php if ($this->session->get('auth')['role'] == 'admin') { ?>
    <script>
        let header = document.body.getElementsByClassName('header');
        header[0].style.transition = 'all .5s';
        header[0].style.background = '#27afa1';
    </script>
<?php } ?>
