<?= $this->getContent() ?>
<div style="width: 500px; margin: 20px auto">
    <div style="text-align: center">
        <h2>Change Password</h2>
    </div>
    <?= $this->tag->form(['/change-password', 'role' => 'form', 'method' => 'post']) ?>
        <div class="control-group">
            <?= $form->label('oldPassword', ['class' => 'control-label']) ?>
            <div class="controls">
                <?= $form->render('oldPassword', ['class' => 'form-control']) ?>
            </div>
        </div>
        <div class="control-group">
            <?= $form->label('password', ['class' => 'control-label']) ?>
            <div class="controls">
                <?= $form->render('password', ['class' => 'form-control']) ?>
            </div>
        </div>
        <div class="control-group">
            <?= $form->label('repeatPassword', ['class' => 'control-label']) ?>
            <div class="controls">
                <?= $form->render('repeatPassword', ['class' => 'form-control']) ?>
            </div>
        </div>
        <?= $this->tag->submitButton(['Change', 'class' => 'btn btn-primary']) ?>
    </form>
</div>