<?= $this->getContent() ?>

<div class="jumbotron", style="text-align: center">
    <h1>Unauthorized</h1>
    <p>You don't have access to this option. Contact an administrator</p>
    <p><?= $this->tag->linkTo(['login', 'Sign in', 'class' => 'btn btn-primary']) ?></p>
</div>