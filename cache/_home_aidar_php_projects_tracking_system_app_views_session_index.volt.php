<?= $this->getContent() ?>
<div class="session">
    <div class="session__title">
        <h2>Authentication:</h2>
    </div>
    <div class="session__fields">
        <?= $this->tag->form(['/sign-in', 'role' => 'form', 'method' => 'post']) ?>
            <input type="hidden" name="<?= $this->security->getTokenKey() ?>" value="<?= $this->security->getToken() ?>">
            <?= $form->label('username') ?>
            <div style="margin: 5px 0 20px">
                <?= $form->render('username', ['class' => 'form-control']) ?>
            </div>
            <?= $form->label('password') ?>
            <div style="margin: 5px 0 20px">
                <?= $form->render('password', ['class' => 'form-control']) ?>
            </div>
            <div>
                <?= $this->tag->submitButton(['Login', ['class' => 'btn btn-primary']]) ?>
            </div>
        </form>
    </div>
</div>