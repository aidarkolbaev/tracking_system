<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= $this->tag->getTitle() ?>
        <script
                src="https://code.jquery.com/jquery-3.3.1.min.js"
                integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
                crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="shortcut icon" type="image/x-icon" href="<?php echo $this->url->get('img/favicon.ico')?>"/>
        <?= $this->tag->stylesheetLink('css/main.css') ?>
    </head>
    <body>
        <?= $this->getContent() ?>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <?= $this->tag->javascriptInclude('js/editTime.js') ?>
        <?= $this->tag->javascriptInclude('js/hideShow.js') ?>
        <?= $this->tag->javascriptInclude('js/secondsConverter.js') ?>
        <script type="text/javascript">

            let years = document.getElementById('year');
            let months = document.getElementById('month');


            years.onchange = function (event) {
                this.form.submit();
            };

            months.onchange = function (event) {
                this.form.submit();
            };


            /*Форматирование секунд в часы и минуты*/
            let seconds = document.getElementsByClassName('seconds-hh-mm');
            for (let i = 0; i < seconds.length; i++) {
                let second = parseInt(seconds[i].innerText);
                seconds[i].innerText = SecondsToHHmm(second);
            }
        </script>
    </body>
</html>
