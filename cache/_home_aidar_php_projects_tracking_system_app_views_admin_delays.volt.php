<?= $this->getContent() ?>

<div class="delays">
    <h2 style="text-align: center">Delays</h2>
    <hr>
    <?php if ($beginning) { ?>
        <h3 style="text-align: center">The work day start at <span style="font-weight: bold"><?= $beginning->value ?></span> o'clock</h3>
        <hr>
    <?php } ?>
    <?= $this->tag->form(['/work-day-beginning', 'method' => 'post']) ?>
        <div style="text-align: center"><label for="work-hours" style="font-size: 16px; margin: 10px 0">Change the beginning of the work day:</label></div>
        <div style="text-align: center">
            <input type="hidden" name="name" value="work-day-beginning">
            <?= $this->tag->select(['value', $hours, 'using' => ['id', 'name']]) ?>
            <input style="margin: 20px" type="submit" value="Change" class="btn btn-primary">
        </div>
    </form>
    <hr>
    <h2 style="text-align: center">Users, who late today:</h2>
    <?php $v161607206813541070361iterated = false; ?><?php $v161607206813541070361iterator = $lateUsers; $v161607206813541070361incr = 0; $v161607206813541070361loop = new stdClass(); $v161607206813541070361loop->self = &$v161607206813541070361loop; $v161607206813541070361loop->length = count($v161607206813541070361iterator); $v161607206813541070361loop->index = 1; $v161607206813541070361loop->index0 = 1; $v161607206813541070361loop->revindex = $v161607206813541070361loop->length; $v161607206813541070361loop->revindex0 = $v161607206813541070361loop->length - 1; ?><?php foreach ($v161607206813541070361iterator as $user) { ?><?php $v161607206813541070361loop->first = ($v161607206813541070361incr == 0); $v161607206813541070361loop->index = $v161607206813541070361incr + 1; $v161607206813541070361loop->index0 = $v161607206813541070361incr; $v161607206813541070361loop->revindex = $v161607206813541070361loop->length - $v161607206813541070361incr; $v161607206813541070361loop->revindex0 = $v161607206813541070361loop->length - ($v161607206813541070361incr + 1); $v161607206813541070361loop->last = ($v161607206813541070361incr == ($v161607206813541070361loop->length - 1)); ?><?php $v161607206813541070361iterated = true; ?>
        <div class="late-user"><div class="index"><?= $v161607206813541070361loop->index ?>.</div><?= $this->tag->linkTo(['/user/' . $user->id, $user->name]) ?> came at <div class="started-time"><?= $user->started ?></div></div>
    <?php $v161607206813541070361incr++; } if (!$v161607206813541070361iterated) { ?>
       <div style="text-align: center; font-weight: bold; margin: 20px">No one was late</div>
    <?php } ?>
</div>


<style>
    .late-user {
        padding: 15px;
        background: white;
        box-shadow: 0 2px 10px #b9b9b9;
        font-size: 16px;
        margin: 10px auto;
        width: 600px;
    }
    .started-time {
        font-weight: bold;
        display: inline-block;
        color: darkred;
    }
    .index {
        margin: 0 10px;
        display: inline-block;
        font-weight: bold;
    }
</style>