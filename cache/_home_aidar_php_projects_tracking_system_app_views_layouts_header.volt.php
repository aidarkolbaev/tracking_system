<?= $this->tag->stylesheetLink('css/admin.css') ?>
<div class="admin__header">
    <div class="admin__header__container">
        <?= $this->tag->linkTo(['/', 'Main menu']) ?>
        <?php if ($this->session->get('auth')['role'] == 'admin') { ?>
            <?= $this->tag->linkTo(['/registration', 'Register user']) ?>
        <?php } ?>
        <?= $this->tag->linkTo(['/logout', 'Logout']) ?>
    </div>
</div>
<?= $this->flash->output() ?>
<div class="container">
    <h3 style="text-align: center; font-size: 30px"><?= $this->session->get('auth')['name'] ?></h3>
    <?php if ($this->session->get('auth')['role'] != 'admin') { ?>
        <div style="text-align: center">
            <button type="button" class="btn btn-primary" id="startStopTime" onclick="startStopTime()">Start</button>
        </div>
    <?php } ?>
    <?= $this->getContent() ?>
</div>

<script>
    function startStopTime() {
        let element = document.getElementById('startStopTime');
        console.log((this));
        let route = '/start-time';
        if (element.innerText.trim().toLowerCase() === 'start') {
            element.innerText = 'Stop';
        } else {
            element.innerText = 'Start';
            route = '/stop-time';
        }
        
            
                
            )#}
            
                
            );#}
    }
</script>
