<?= $this->getContent() ?>
<div class="user-info">
    <div style="text-align: right">
        <?php if ($user->active == 1) { ?>
            <?= $this->tag->form(['/delete/user/' . $user->id]) ?>
                <button type="submit" class="btn btn-danger" onclick="this.form.submit()">Delete User</button>
            </form>
        <?php } else { ?>
            <div class="deleted">DELETED</div>
        <?php } ?>
    </div>
    <h2>User info:</h2>
    <div class="info">Full name: <span class="bold"><?= $user->name ?></span></div>
    <div class="info">Username: <span class="bold"><?= $user->username ?></span></div>
    <div class="info">Email: <span class="bold"><?= $user->email ?></span></div>
</div>
<div class="user-delays">
    <h3 style="text-align: center; color: darkred; margin: 20px"><?= $user->name ?> was late <?= $this->length($delays) ?> times in current month:</h3>
    <?php $v102384307832141179451iterated = false; ?><?php $v102384307832141179451iterator = $delays; $v102384307832141179451incr = 0; $v102384307832141179451loop = new stdClass(); $v102384307832141179451loop->self = &$v102384307832141179451loop; $v102384307832141179451loop->length = count($v102384307832141179451iterator); $v102384307832141179451loop->index = 1; $v102384307832141179451loop->index0 = 1; $v102384307832141179451loop->revindex = $v102384307832141179451loop->length; $v102384307832141179451loop->revindex0 = $v102384307832141179451loop->length - 1; ?><?php foreach ($v102384307832141179451iterator as $delay) { ?><?php $v102384307832141179451loop->first = ($v102384307832141179451incr == 0); $v102384307832141179451loop->index = $v102384307832141179451incr + 1; $v102384307832141179451loop->index0 = $v102384307832141179451incr; $v102384307832141179451loop->revindex = $v102384307832141179451loop->length - $v102384307832141179451incr; $v102384307832141179451loop->revindex0 = $v102384307832141179451loop->length - ($v102384307832141179451incr + 1); $v102384307832141179451loop->last = ($v102384307832141179451incr == ($v102384307832141179451loop->length - 1)); ?><?php $v102384307832141179451iterated = true; ?>
        <div style="position: relative">
            <div style="margin: 5px; color: darkred"><?= $v102384307832141179451loop->index ?>) <?= $delay->created_at ?></div>
            <div style="position: absolute; right: 20px; top: 0">
                <?= $this->tag->form(['/delete/delay/' . $delay->id]) ?>
                    <input type="submit" class="btn btn-danger" value="Delete">
                </form>
            </div>
        </div>
        <hr>
    <?php $v102384307832141179451incr++; } if (!$v102384307832141179451iterated) { ?>
        <hr>
        <div style="text-align: center; color: darkgreen">Good!</div>
        <hr>
    <?php } ?>
</div>

<style>
    .user-info {
        width: 800px;
        margin: 20px auto;
        padding: 20px;
        background: #f0f0f0;
        border-radius: 5px;
    }
    .info {
        margin: 20px 0;
        font-weight: 500;
        font-size: 20px;
    }
    .bold {
        font-weight: bold;
        margin: 0 10px;
    }
    .deleted {
        background: rgba(158, 0, 0, 0.29);
        font-size: 22px;
        color: darkred;
        font-weight: bold;
        text-align: center;
        padding: 5px;
    }
    .user-delays {
        width: 800px;
        margin: 20px auto;
        padding: 20px;
    }
</style>